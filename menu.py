import os
from game import *

def _show_menu(message):
	os.system('clear')

	if message:
		print('--%s--' % message)
	print(' 1. New Game')
	print(' 2. Exit')

def _is_valid(cmd):
	return cmd in ['1', '2']

def _execute(cmd):
	if cmd == '1':
		return _game_play()

	if cmd == '2':
		exit()

def init():
	message = ''
	while True:
		_show_menu(message)
		cmd = input("Choose: ")
		message = ''
		if _is_valid(cmd):
			message = _execute(cmd)
